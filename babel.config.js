module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      require.resolve('babel-plugin-module-resolver'),
      {
        alias: {
          store: './src/redux/store',
          router: './src/router',
          colors: './src/constant/colors',
          action: './src/redux/actions',
        },
      },
    ],
  ],
};
