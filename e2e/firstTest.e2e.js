describe('Example', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  // beforeEach(async () => {
  //   await device.reloadReactNative();
  // });

  test('Should splash screen displayed', async () => {
    await expect(element(by.id('splash-screen'))).toBeVisible();
  });
});
