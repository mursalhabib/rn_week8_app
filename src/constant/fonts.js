export const FONTS = {
  REGULAR: 'Lato-Regular',
  SEMIBOLD: 'Lato-SemiBold',
  BOLD: 'Lato-Bold',
  BLACK: 'Lato-Black',
};
