export const COLORS = {
  WHITE: '#ffffff',
  BLACK: '#2d3035',
  RED: 'red',
  ORANGE: 'orange',
  DARK: '#22252a',
  GRAY: 'gray',
  TEAL: '#48c2c3',
  LIGHT_DARK: '#dddddd',
  LIGHT_GRAY: 'lightgray',
  LIGHT_GRAY_100: '#F5F5F5',
  LIGHT_TEAL: '#b2d8d8',
};
