import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  Home,
  Splash,
  Search,
  Bookmark,
  Login,
  Register,
  Detail,
  Verification,
} from '../pages';
import {connect} from 'react-redux';
import {addBookmarkBook} from '../redux/actions/AddBookmarkAction';
import {notifikasi} from '../Notif';
import Share from 'react-native-share';
import LottieView from 'lottie-react-native';

export class Router extends Component {
  mainApp = () => {
    const Tab = createBottomTabNavigator();
    return (
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused}) => {
            if (route.name === 'Home')
              return focused ? (
                <Icon name="book-open" size={30} color="#48c2c3" />
              ) : (
                <Icon name="book-open-outline" size={30} color="#dddddd" />
              );
            if (route.name === 'Search')
              return focused ? (
                <Icon name="file-search" size={30} color="#48c2c3" />
              ) : (
                <Icon name="file-search-outline" size={30} color="#dddddd" />
              );
            if (route.name === 'Favorite')
              return focused ? (
                <Icon name="heart" size={30} color="#48c2c3" />
              ) : (
                <Icon name="heart-outline" size={30} color="#dddddd" />
              );
          },
          tabBarButton: props => (
            <TouchableOpacity activeOpacity={0.5} {...props} />
          ),
        })}
        tabBarOptions={{
          activeTintColor: '#48c2c3',
          inactiveTintColor: '#6f7781',
          style: {
            height: 60,
            elevation: 0,
            borderTopColor: '#fff',
            paddingTop: 7,
            paddingBottom: 7,
            backgroundColor: '#fff',
          },
          keyboardHidesTabBar: true,
        }}>
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Search" component={Search} />
        <Tab.Screen name="Favorite" component={Bookmark} />
      </Tab.Navigator>
    );
  };

  handleBookmark = () => {
    this.animation.play(0, 90);
    const add = this.props.bookDetail.id;
    this.props.addBookmarkBook(add);
    this.handleNotification();
  };

  handleNotification = () => {
    notifikasi.configure();
    notifikasi.buatChannel('1');
    notifikasi.kirimNotif(
      '1',
      'MyBookApp',
      `"${this.props.bookDetail.title}" successfully added to favorite`,
    );
  };

  handleShare = async () => {
    const shareOptions = {
      message: `Hey, check out this Book!\n${this.props.bookDetail.title}`,
    };
    try {
      await Share.open(shareOptions);
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    console.log(this.props.love, 'ini data love');
    console.log(this.props.bookDetail, 'ini data bookmark');
    const Stack = createStackNavigator();
    return (
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Verification"
          component={Verification}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="mainApp"
          component={this.mainApp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Detail"
          component={Detail}
          options={{
            title: '',
            headerTransparent: true,
            headerTintColor: '#6f7781',
            headerRight: () => (
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                {this.props.bookMark.some(
                  temp => temp._id === this.props.bookDetail.id,
                ) ? (
                  <Icon
                    color="#F07470"
                    size={27}
                    name="heart"
                    style={{
                      marginRight: 30,
                      borderRadius: 10,
                      padding: 5,
                    }}
                  />
                ) : (
                  <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={this.handleBookmark}>
                    <LottieView
                      loop={false}
                      style={{
                        width: 100,
                        height: 100,
                      }}
                      ref={animation => {
                        this.animation = animation;
                      }}
                      source={require('../assets/lottie/heart.json')}
                    />
                  </TouchableOpacity>
                )}

                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={this.handleShare}>
                  <Icon
                    color="#6f7781"
                    size={27}
                    name="share"
                    style={{
                      marginRight: 20,
                      borderRadius: 10,
                      padding: 4,
                    }}
                  />
                </TouchableOpacity>
              </View>
            ),
          }}
        />
      </Stack.Navigator>
    );
  }
}

const mapStateToProps = state => {
  const userData = state.login.user;
  const bookDetail = state.allBook.detailBook;
  const bookMark = state.bookmark.bookmarkGet;
  return {
    userData,
    bookDetail,
    bookMark,
  };
};

const mapDispatchToProps = {addBookmarkBook};

export default connect(mapStateToProps, mapDispatchToProps)(Router);
