import {
  POST_LOGIN_START,
  POST_LOGIN_START_SUCCESS,
  POST_LOGIN_START_FAILED,
  LOGOUT,
} from 'action/LoginAction';
import {
  REGISTER_START,
  REGISTER_SUCCESS,
  REGISTER_FAILED,
} from 'action/RegisterAction';

const initialState = {
  user: {},
  isLoading: false,
  isAuthenticate: false,
  success: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case POST_LOGIN_START:
      return {...state, isLoading: true, isAuthenticate: false};

    case POST_LOGIN_START_SUCCESS:
      return {
        ...state,
        isLoading: false,
        user: action.loginInfo,
        isAuthenticate: true,
        success: true,
      };

    case POST_LOGIN_START_FAILED:
      return {...state, isLoading: false};

    case LOGOUT:
      return {...state, isAuthenticate: false, success: false};

    case REGISTER_START:
      return {...state, isLoading: true, isAuthenticate: false};

    case REGISTER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        user: action.registerInfo,
        success: true,
      };

    case REGISTER_FAILED:
      return {...state, isLoading: false};
    default:
      return state;
  }
};
