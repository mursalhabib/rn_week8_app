import {
  GET_BOOKMARK_START,
  GET_BOOKMARK_SUCCESS,
  GET_BOOKMARK_FAILED,
} from 'action/BookmarkAction';
import {
  ADD_BOOKMARK_START,
  ADD_BOOKMARK_SUCCESS,
  ADD_BOOKMARK_FAILED,
} from 'action/AddBookmarkAction';

const initialState = {
  bookmarked: {},
  bookmarkGet: [],
  isLoading: false,
};

export default (state = initialState, action) => {
  // console.log(state, 'ini state book');
  // console.log(action, 'ini action yg kepanggil');
  switch (action.type) {
    case GET_BOOKMARK_START:
      return {...state, isLoading: true};

    case GET_BOOKMARK_SUCCESS:
      return {
        ...state,
        isLoading: false,
        bookmarkGet: action.bookmarkData.data,
      };

    case GET_BOOKMARK_FAILED:
      return {...state, isLoading: false};

    case ADD_BOOKMARK_START:
      return {...state, isLoading: true};

    case ADD_BOOKMARK_SUCCESS:
      return {
        ...state,
        isLoading: false,
        bookmarked: action.addBookmark,
      };

    case ADD_BOOKMARK_FAILED:
      return {...state, isLoading: false};

    default:
      return state;
  }
};
