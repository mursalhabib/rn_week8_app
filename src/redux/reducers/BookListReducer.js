import {
  GET_BOOK_START,
  GET_BOOK_SUCCESS,
  GET_BOOK_FAILED,
  GET_MORE_START,
  GET_MORE_SUCCESS,
  GET_MORE_FAILED,
} from 'action/BookListAction';
import {
  SEARCH_BOOK_START,
  SEARCH_BOOK_SUCCESS,
  SEARCH_BOOK_FAILED,
} from 'action/SearchAction';
import {
  DETAIL_START,
  DETAIL_SUCCESS,
  DETAIL_FAILED,
} from 'action/DetailBookAction';

const initialState = {
  detailBook: {},
  books: [],
  query: [],
  input: '',
  searchTimer: null,
  page: 1,
  isLoading: false,
};

export default (state = initialState, action) => {
  // console.log(state, 'ini state book');
  // console.log(action, 'ini action yg kepanggil');
  switch (action.type) {
    case GET_BOOK_START:
      return {...state, isLoading: true, page: 1};

    case GET_BOOK_SUCCESS:
      return {
        ...state,
        isLoading: false,
        books: action.getBookList,
        page: 2,
      };

    case GET_BOOK_FAILED:
      return {...state, isLoading: false};

    case GET_MORE_START:
      return {...state, isLoading: true};

    case GET_MORE_SUCCESS:
      return {
        ...state,
        isLoading: false,
        books: [...(state.books || []), ...action.getMoreBook],
        page: state.page + 1,
      };

    case GET_MORE_FAILED:
      return {...state, isLoading: false};

    case SEARCH_BOOK_START:
      return {...state, isLoading: true};

    case SEARCH_BOOK_SUCCESS:
      return {
        ...state,
        isLoading: false,
        query: action.fetchBook.data.results,
      };

    case SEARCH_BOOK_FAILED:
      return {...state, isLoading: false};

    case DETAIL_START:
      return {...state, isLoading: true};

    case DETAIL_SUCCESS:
      return {
        ...state,
        isLoading: false,
        detailBook: action.detailBookData.data,
      };

    case DETAIL_FAILED:
      return {...state, isLoading: false};

    default:
      return state;
  }
};
