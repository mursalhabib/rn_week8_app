import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import login from '../redux/reducers/LoginReducer';
import allBook from '../redux/reducers/BookListReducer';
import bookmark from '../redux/reducers/BookmarkReducer';
import {createLogger} from 'redux-logger';
import {persistStore, persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';

const logger = createLogger({});

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const allReducer = combineReducers({
  login,
  allBook,
  bookmark,
});

const permanentReducer = persistReducer(persistConfig, allReducer);

export const bookStore = createStore(
  permanentReducer,
  applyMiddleware(thunk, logger),
);

export const permanentStore = persistStore(bookStore);
