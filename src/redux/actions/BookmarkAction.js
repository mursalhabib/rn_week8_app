import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {ToastAndroid} from 'react-native';

export const GET_BOOKMARK_START = 'GET_BOOKMARK_START';
export const GET_BOOKMARK_SUCCESS = 'GET_BOOKMARK_SUCCESS';
export const GET_BOOKMARK_FAILED = 'GET_BOOKMARK_FAILED';

export const bookmarkBook = query => async dispatch => {
  try {
    dispatch({
      type: GET_BOOKMARK_START,
    });
    const value = await AsyncStorage.getItem('token-user');
    const bookmarkData = await axios.get(
      `http://code.aldipee.com/api/v1/books/my-favorite`,
      {headers: {Authorization: `Bearer ${value}`}},
    );
    dispatch({
      type: GET_BOOKMARK_SUCCESS,
      bookmarkData,
    });

    console.log(bookmarkData, 'ini data ngambil dr bookmark');
    // console.log(bookmarkData.status);
    // console.log(bookmarkData.data.results);
  } catch (error) {
    ToastAndroid.showWithGravity(
      'fetch failed',
      ToastAndroid.LONG,
      ToastAndroid.TOP,
    );
    dispatch({
      type: GET_BOOKMARK_FAILED,
    });
  }
};
