import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Alert, Text, ToastAndroid} from 'react-native';

export const POST_LOGIN_START = 'POST_LOGIN_START';
export const POST_LOGIN_START_SUCCESS = 'POST_LOGIN_START_SUCCESS';
export const POST_LOGIN_START_FAILED = 'POST_LOGIN_START_FAILED';
export const LOGOUT = 'LOGOUT';

export const getDataLogin = (user, callback) => async dispatch => {
  try {
    dispatch({
      type: POST_LOGIN_START,
    });
    const loginInfo = await axios.post(
      'http://code.aldipee.com/api/v1/auth/login',
      user,
    );
    console.log(loginInfo);
    if (loginInfo.status === 200) {
      dispatch({
        type: POST_LOGIN_START_SUCCESS,
        loginInfo,
      });
      await AsyncStorage.setItem(
        'token-user',
        loginInfo.data.tokens.access.token,
      );
      callback({isSuccess: true});
    }
  } catch (error) {
    dispatch({
      type: POST_LOGIN_START_FAILED,
    });
    ToastAndroid.showWithGravity(
      'incorrect email or password',
      ToastAndroid.LONG,
      ToastAndroid.TOP,
    );
  }
};

export const logoutAction = history => {
  return {type: LOGOUT};
};
