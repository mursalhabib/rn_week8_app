import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {ToastAndroid} from 'react-native';

export const SEARCH_BOOK_START = 'SEARCH_BOOK_START';
export const SEARCH_BOOK_SUCCESS = 'SEARCH_BOOK_SUCCESS';
export const SEARCH_BOOK_FAILED = 'SEARCH_BOOK_FAILED';
export const TRUNCATE_TEXT = 'TRUNCATE_TEXT';
export const TO_UPPER = 'TO_UPPER';

export const queryBooks = query => async dispatch => {
  try {
    dispatch({
      type: SEARCH_BOOK_START,
    });
    const value = await AsyncStorage.getItem('token-user');
    const fetchBook = await axios.get(
      `http://code.aldipee.com/api/v1/books?title=${query}`,
      {headers: {Authorization: `Bearer ${value}`}},
    );
    dispatch({
      type: SEARCH_BOOK_SUCCESS,
      fetchBook,
    });

    console.log(fetchBook, 'ini data hasil search');
    console.log(fetchBook.status);
    console.log(fetchBook.data.results);
  } catch (error) {
    ToastAndroid.showWithGravity(
      'fetch failed',
      ToastAndroid.LONG,
      ToastAndroid.TOP,
    );
    dispatch({
      type: SEARCH_BOOK_FAILED,
    });
  }
};

export const truncate = input => dispatch => {
  dispatch({
    type: TRUNCATE_TEXT,
  });
  if (input.length > 25) {
    return input.substring(0, 25) + '..';
  }
  return input;
};

export const toUpper = text => dispatch => {
  dispatch({
    type: TO_UPPER,
  });
  if (text.length != 0) {
    const sentence = text.split(' ');
    const word = sentence.map(
      x => x[0].toUpperCase() + x.substr(1).toLowerCase(),
    );
    return word.join(' ');
  }
};
