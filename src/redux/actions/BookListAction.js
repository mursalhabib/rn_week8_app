import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {ToastAndroid} from 'react-native';

export const GET_BOOK_START = 'GET_BOOK_START';
export const GET_BOOK_SUCCESS = 'GET_BOOK_SUCCESS';
export const GET_BOOK_FAILED = 'GET_BOOK_FAILED';
export const GET_MORE_START = 'GET_MORE_START';
export const GET_MORE_SUCCESS = 'GET_MORE_SUCCESS';
export const GET_MORE_FAILED = 'GET_MORE_FAILED';
export const IS_CLOSE_TO_BOTTOM = 'IS_CLOSE_TO_BOTTOM';

export const getAllBookList = page => async dispatch => {
  try {
    dispatch({
      type: GET_BOOK_START,
      page,
    });
    const value = await AsyncStorage.getItem('token-user');
    const getBookList = await axios.get(
      `http://code.aldipee.com/api/v1/books`,
      {headers: {Authorization: `Bearer ${value}`}},
    );
    dispatch({
      type: GET_BOOK_SUCCESS,
      getBookList: getBookList.data.results,
    });

    console.log(getBookList.data.page, 'ini data book utk liat page');
    // console.log(getBookList.status);
    // console.log(getBookList.data.results);
  } catch (error) {
    ToastAndroid.showWithGravity(
      'fetch failed',
      ToastAndroid.LONG,
      ToastAndroid.TOP,
    );
    dispatch({
      type: GET_BOOK_FAILED,
    });
  }
};

export const getMoreBook = page => async dispatch => {
  try {
    dispatch({
      type: GET_MORE_START,
      page,
    });
    const value = await AsyncStorage.getItem('token-user');
    const getMoreBook = await axios.get(
      `http://code.aldipee.com/api/v1/books?page=${page}`,
      {headers: {Authorization: `Bearer ${value}`}},
    );
    dispatch({
      type: GET_MORE_SUCCESS,
      getMoreBook: getMoreBook.data.results,
    });

    console.log(getMoreBook.data.page, 'ini data book utk load more');
    // console.log(getBookList.status);
    // console.log(getBookList.data.results);
  } catch (error) {
    ToastAndroid.showWithGravity(
      'fetch failed',
      ToastAndroid.LONG,
      ToastAndroid.TOP,
    );
    dispatch({
      type: GET_MORE_FAILED,
    });
  }
};

export const isCloseToBottom =
  ({layoutMeasurement, contentOffset, contentSize}) =>
  dispatch => {
    dispatch({
      type: IS_CLOSE_TO_BOTTOM,
    });
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };
