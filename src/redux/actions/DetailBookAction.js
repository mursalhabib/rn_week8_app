import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {ToastAndroid} from 'react-native';

export const DETAIL_START = 'DETAIL_START';
export const DETAIL_SUCCESS = 'DETAIL_SUCCESS';
export const DETAIL_FAILED = 'DETAIL_FAILED';

export const detailBook = detail => async dispatch => {
  try {
    dispatch({
      type: DETAIL_START,
    });
    const value = await AsyncStorage.getItem('token-user');
    const detailBookData = await axios.get(
      `http://code.aldipee.com/api/v1/books/${detail}`,
      {headers: {Authorization: `Bearer ${value}`}},
    );
    dispatch({
      type: DETAIL_SUCCESS,
      detailBookData,
    });

    console.log(detailBookData, 'ini data hasil search');
  } catch (error) {
    ToastAndroid.showWithGravity(
      'fetch failed',
      ToastAndroid.LONG,
      ToastAndroid.TOP,
    );
    dispatch({
      type: DETAIL_FAILED,
    });
  }
};
