import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {ToastAndroid} from 'react-native';

export const ADD_BOOKMARK_START = 'ADD_BOOKMARK_START';
export const ADD_BOOKMARK_SUCCESS = 'ADD_BOOKMARK_SUCCESS';
export const ADD_BOOKMARK_FAILED = 'ADD_BOOKMARK_FAILED';

export const addBookmarkBook = bookId => async dispatch => {
  try {
    dispatch({
      type: ADD_BOOKMARK_START,
    });
    const value = await AsyncStorage.getItem('token-user');
    const addBookmark = await axios.post(
      'http://code.aldipee.com/api/v1/books/my-favorite',
      {bookId},
      {headers: {Authorization: `Bearer ${value}`}},
    );
    if (addBookmark.status === 201) {
      dispatch({
        type: ADD_BOOKMARK_SUCCESS,
        addBookmark,
      });
      //   callback({isSuccess: true});
    }
    console.log(addBookmark, 'ini data bookmark');
  } catch (error) {
    dispatch({
      type: ADD_BOOKMARK_FAILED,
    });
    ToastAndroid.showWithGravity(
      error.message,
      ToastAndroid.LONG,
      ToastAndroid.TOP,
    );
  }
};

export const logoutAction = history => {
  return {type: LOGOUT};
};
