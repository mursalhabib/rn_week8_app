import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Alert, Text, ToastAndroid} from 'react-native';

export const REGISTER_START = 'REGISTER_START';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILED = 'REGISTER_FAILED';

export const getDataRegister = newuser => async dispatch => {
  try {
    dispatch({
      type: REGISTER_START,
    });
    const registerInfo = await axios.post(
      'http://code.aldipee.com/api/v1/auth/register',
      newuser,
    );
    if (registerInfo.status === 201) {
      dispatch({
        type: REGISTER_SUCCESS,
        registerInfo,
      });
    }

    // console.log(registerInfo);
    // console.log(loginInfo.status);
    // console.log(loginInfo.data.user.name);
  } catch (error) {
    ToastAndroid.showWithGravity(
      'email has already taken',
      ToastAndroid.LONG,
      ToastAndroid.TOP,
    );
    dispatch({
      type: REGISTER_FAILED,
    });
  }
};
