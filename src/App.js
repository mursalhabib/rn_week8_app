import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import Router from './router';
import {Provider} from 'react-redux';
import {bookStore, permanentStore} from 'store';
import {PersistGate} from 'redux-persist/integration/react';

export class App extends Component {
  render() {
    return (
      <Provider store={bookStore}>
        <PersistGate loading={null} persistor={permanentStore}>
          <NavigationContainer>
            <Router />
          </NavigationContainer>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
