import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {connect} from 'react-redux';
import {getDataLogin} from 'action/LoginAction';
import LottieView from 'lottie-react-native';
import {Styles} from './style';

export class Splash extends Component {
  componentDidMount() {
    this.animation.play();
    setTimeout(() => {
      if (this.props.login.isAuthenticate === true) {
        this.props.navigation.replace('mainApp');
      } else {
        this.props.navigation.replace('Login');
      }
    }, 3000);
  }
  render() {
    return (
      <View style={Styles.container} testID="splash-screen">
        <LottieView
          style={Styles.logo}
          ref={animation => {
            this.animation = animation;
          }}
          source={require('../../assets/lottie/cube.json')}
        />
        <Text style={Styles.name}>C U E B O O K</Text>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    login: state.login,
  };
};

const mapDispatchToProps = {
  getDataLogin,
};

export default connect(mapStateToProps, mapDispatchToProps)(Splash);
