import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../constant/colors';
import {FONTS} from '../../constant/fonts';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';

const winHeight = Dimensions.get('window').height;
export const Styles = StyleSheet.create({
  container: {flex: 1, justifyContent: 'center', alignItems: 'center'},
  logo: {width: scale(300), height: verticalScale(300)},
  name: {
    fontFamily: FONTS.REGULAR,
    fontSize: moderateScale(15),
    color: COLORS.GRAY,
  },
});
