import React, {Component} from 'react';
import {
  Text,
  View,
  Keyboard,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import {getDataLogin} from 'action/LoginAction';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Styles} from './style';
import {COLORS} from 'colors';

export class Login extends Component {
  constructor() {
    super();
    this.state = {
      showPassword: true,
    };
  }
  handleChangeEmail = email => {
    this.setState({email});
  };

  handleChangePassword = password => {
    this.setState({password});
  };

  handleSubmitItem = () => {
    const dataBody = {
      email: this.state.email,
      password: this.state.password,
    };
    this.props.getDataLogin(dataBody, datadrAction => {
      if (datadrAction.isSuccess === true) {
        this.props.navigation.replace('mainApp');
      }
    });
  };

  togglePassword = () => {
    this.setState({showPassword: !this.state.showPassword});
  };

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={Styles.container}>
          <View style={Styles.form}>
            <Text style={Styles.textLogin}>Sign in</Text>
            <TextInput
              style={Styles.emailInput}
              placeholder="email"
              value={this.state.email}
              placeholderTextColor={COLORS.GRAY}
              keyboardType="email-address"
              onChangeText={this.handleChangeEmail}
            />
            <View style={Styles.passwordContainer}>
              <TextInput
                style={Styles.passwordInput}
                placeholder="password"
                secureTextEntry={this.state.showPassword}
                value={this.state.password}
                placeholderTextColor={COLORS.GRAY}
                onChangeText={this.handleChangePassword}
              />
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={this.togglePassword}
                style={Styles.btnToggle}>
                {this.state.showPassword === true ? (
                  <Icon name="eye-off-outline" size={25} color={COLORS.TEAL} />
                ) : (
                  <Icon name="eye-outline" size={25} color={COLORS.TEAL} />
                )}
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              onPress={() =>
                Alert.alert(
                  'Forgot your password?',
                  'Relax and try to remember your password',
                )
              }>
              <Text style={Styles.forgot}>Forgot Password?</Text>
            </TouchableOpacity>
          </View>
          <View style={Styles.question}>
            <Text style={Styles.quest}>Don't have an account? </Text>
            <TouchableOpacity
              style={Styles.register}
              onPress={() => this.props.navigation.navigate('Register')}>
              <Text style={Styles.quest2}>Register</Text>
            </TouchableOpacity>
          </View>
          {!this.state.password ||
          !this.state.email ||
          this.props.user.isLoading === true ? (
            <View>
              <TouchableOpacity
                style={Styles.buttonLoginDisabled}
                disabled={true}>
                <Text style={Styles.textBtnLogin}>sign in</Text>
              </TouchableOpacity>
            </View>
          ) : (
            <View>
              <TouchableOpacity
                onPress={this.handleSubmitItem}
                style={Styles.buttonLogin}>
                <Text style={Styles.textBtnLogin}>sign in</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.login,
  };
};

const mapDispatchToProps = {
  getDataLogin,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
