import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../constant/colors';
import {FONTS} from '../../constant/fonts';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';

const winHeight = Dimensions.get('window').height;
export const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.WHITE,
    paddingHorizontal: scale(40),
  },
  form: {
    marginTop: winHeight * verticalScale(0.25),
  },
  textLogin: {
    color: COLORS.BLACK,
    fontFamily: FONTS.BLACK,
    fontSize: moderateScale(24),
    marginBottom: verticalScale(25),
  },
  emailInput: {
    backgroundColor: COLORS.LIGHT_DARK,
    padding: moderateScale(10),
    borderRadius: 17,
    paddingHorizontal: scale(30),
    fontFamily: FONTS.REGULAR,
    color: COLORS.BLACK,
    fontSize: moderateScale(17),
    marginBottom: verticalScale(15),
  },
  passwordContainer: {flexDirection: 'row'},
  passwordInput: {
    flex: 1,
    backgroundColor: COLORS.LIGHT_DARK,
    padding: moderateScale(10),
    borderRadius: 17,
    paddingHorizontal: scale(30),
    color: COLORS.BLACK,
    fontFamily: FONTS.REGULAR,
    fontSize: moderateScale(17),
    marginBottom: verticalScale(10),
  },
  btnToggle: {position: 'absolute', right: scale(20), top: verticalScale(10)},
  buttonLoginDisabled: {
    backgroundColor: COLORS.LIGHT_TEAL,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 7,
    fontFamily: FONTS.BLACK,
    height: verticalScale(50),
    borderRadius: 15,
    marginTop: verticalScale(15),
  },
  buttonLogin: {
    backgroundColor: COLORS.TEAL,
    alignItems: 'center',
    justifyContent: 'center',
    padding: moderateScale(7),
    height: verticalScale(50),
    borderRadius: 15,
    marginTop: verticalScale(15),
  },
  textBtnLogin: {
    fontSize: moderateScale(19),
    color: COLORS.WHITE,
    fontFamily: FONTS.BLACK,
  },
  question: {
    flexDirection: 'row',
    marginTop: winHeight * verticalScale(0.27),
    justifyContent: 'center',
  },
  quest: {
    color: COLORS.BLACK,
    fontFamily: FONTS.REGULAR,
  },
  quest2: {
    color: COLORS.BLACK,
    fontFamily: FONTS.BLACK,
  },
  forgot: {
    color: COLORS.LIGHT_GRAY,
    fontSize: moderateScale(12),
    fontFamily: FONTS.REGULAR,
  },
});
