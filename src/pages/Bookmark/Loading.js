import React, {Component} from 'react';
import {Text, View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

export class Loading extends Component {
  render() {
    return (
      <View>
        <SkeletonPlaceholder>
          <View style={{alignItems: 'center', marginTop: 20}}>
            <View style={{width: 150, height: 30, borderRadius: 4}} />
          </View>
          <View style={{marginTop: 25}}>
            <View
              style={{
                width: 370,
                height: 130,
                borderRadius: 4,
              }}
            />
          </View>
          <View style={{marginTop: 10}}>
            <View
              style={{
                width: 370,
                height: 130,
                borderRadius: 4,
                marginBottom: 10,
              }}
            />
          </View>
          <View style={{marginTop: 10}}>
            <View
              style={{
                width: 370,
                height: 130,
                borderRadius: 4,
                marginBottom: 10,
              }}
            />
          </View>
          <View style={{marginTop: 10}}>
            <View
              style={{
                width: 370,
                height: 130,
                borderRadius: 4,
                marginBottom: 10,
              }}
            />
          </View>
        </SkeletonPlaceholder>
      </View>
    );
  }
}

export default Loading;
