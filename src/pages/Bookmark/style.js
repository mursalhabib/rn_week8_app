import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../constant/colors';
import {FONTS} from '../../constant/fonts';

const winHeight = Dimensions.get('window').height;
export const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.WHITE,
    paddingHorizontal: 20,
  },
  titleFav: {
    marginTop: 20,
    marginBottom: 30,
    textAlign: 'center',
    fontFamily: FONTS.BOLD,
    fontSize: 17,
  },
  img: {
    marginVertical: 5,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: COLORS.LIGHT_GRAY_100,
    padding: 10,
    borderRadius: 10,
  },
  cover: {width: 72, height: 108},
  rowCon: {
    marginLeft: 15,
    width: '80%',
  },
  txtTitle: {
    fontFamily: FONTS.BOLD,
    fontSize: 17,
    marginBottom: 3,
  },
  star: {flexDirection: 'row', marginTop: 10},
  noBook: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
