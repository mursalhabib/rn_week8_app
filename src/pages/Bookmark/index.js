import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  RefreshControl,
  TouchableOpacity,
  Image,
} from 'react-native';
import {connect} from 'react-redux';
import {logoutAction} from 'action/LoginAction';
import {bookmarkBook} from 'action/BookmarkAction';
import {Styles} from './style';
import Stars from 'react-native-stars';
import Icon from 'react-native-vector-icons/FontAwesome';
import Loading from './Loading';
import {COLORS} from 'colors';

export class Bookmark extends Component {
  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.props.bookmarkBook();
    });
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  render() {
    return (
      <ScrollView
        style={Styles.container}
        refreshControl={
          <RefreshControl
            refreshing={this.props.load}
            onRefresh={() => this.props.bookmarkBook()}
          />
        }>
        {this.props.load ? (
          <Loading />
        ) : (
          <View>
            <Text style={Styles.titleFav}>Your Favorite Books</Text>
            {this.props.userBookmark.length ? (
              this.props.userBookmark.map(item => (
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate('Detail', item._id)
                  }
                  style={Styles.img}>
                  <Image
                    source={{uri: item.cover_image}}
                    style={Styles.cover}
                  />
                  <View style={Styles.rowCon}>
                    <Text style={Styles.txtTitle}>{item.title}</Text>
                    <Text>{item.author}</Text>
                    <View style={Styles.star}>
                      <Stars
                        disabled={true}
                        rating={parseInt(item.average_rating) / 2}
                        count={5}
                        half={true}
                        fullStar={
                          <Icon name="star" size={15} color={COLORS.ORANGE} />
                        }
                        emptyStar={
                          <Icon name="star-o" size={15} color={COLORS.ORANGE} />
                        }
                        halfStar={
                          <Icon
                            name="star-half-full"
                            size={15}
                            color={COLORS.ORANGE}
                          />
                        }
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              ))
            ) : (
              <View style={Styles.noBook}>
                <Text>No book added to favorite yet</Text>
              </View>
            )}
          </View>
        )}
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  const userData = state.login.user;
  const userBookmark = state.bookmark.bookmarkGet;
  const load = state.bookmark.isLoading;
  return {
    userData,
    userBookmark,
    load,
  };
};

const mapDispatchToProps = {
  logoutAction,
  bookmarkBook,
};

export default connect(mapStateToProps, mapDispatchToProps)(Bookmark);
