import Bookmark from './Bookmark';
import Detail from './Detail';
import Home from './Home';
import Login from './Login';
import Register from './Register';
import Search from './Search';
import Splash from './Splash';
import Verification from './Verification';

export {Bookmark, Home, Login, Register, Splash, Detail, Search, Verification};
