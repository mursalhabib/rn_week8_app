import React, {Component} from 'react';
import {
  Text,
  View,
  TextInput,
  Button,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import {queryBooks, truncate, toUpper} from 'action/SearchAction';
import {getDataLogin} from 'action/LoginAction';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Styles} from './style';
import {COLORS} from 'colors';

export class Search extends Component {
  handleQuery = query => {
    if (this.props.timer) {
      clearTimeout(this.props.timer);
    }
    this.setState({query: this.props.q});
    this.setState({
      query: setTimeout(() => {
        this.props.queryBooks(query);
      }, 100),
    });
  };

  componentDidMount() {
    this.props.bookData;
  }

  render() {
    return (
      <ScrollView style={Styles.container}>
        <View>
          <Text style={Styles.textSearch}> Search Page</Text>
          <View>
            <TextInput
              ref={input => {
                this.textInput = input;
              }}
              value={this.props.q}
              placeholder="search here.."
              onChangeText={this.handleQuery}
              style={Styles.searchInput}
            />
            <TouchableOpacity
              onPress={() => this.textInput.clear()}
              style={Styles.icon}>
              <Icon name="close" size={30} color={COLORS.GRAY} />
            </TouchableOpacity>
          </View>

          <View style={Styles.result}>
            {!this.props.bookData.length ? (
              <Text></Text>
            ) : (
              this.props.bookData.map(item => {
                return (
                  <TouchableOpacity
                    style={Styles.q}
                    key={item.id}
                    onPress={() =>
                      this.props.navigation.navigate('Detail', item.id)
                    }>
                    <Image
                      source={{uri: item.cover_image}}
                      style={Styles.img}
                    />
                    <Text style={Styles.txtTitle}>
                      {this.props.truncate(item.title)}
                    </Text>
                  </TouchableOpacity>
                );
              })
            )}
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  const userData = state.login.user;
  const bookData = state.allBook.query;
  const q = state.allBook.q;
  const timer = state.allBook.searchTimer;
  const load = state.allBook.isLoading;
  return {
    userData,
    bookData,
    load,
    q,
    timer,
  };
};

const mapDispatchToProps = {
  queryBooks,
  getDataLogin,
  truncate,
  toUpper,
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
