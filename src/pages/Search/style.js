import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../constant/colors';
import {FONTS} from '../../constant/fonts';

const winHeight = Dimensions.get('window').height;
export const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    paddingHorizontal: 20,
  },
  textSearch: {
    color: '#2d3035',
    fontSize: 17,
    textAlign: 'center',
    marginVertical: 10,
    fontFamily: 'Lato-Bold',
  },
  searchInput: {
    backgroundColor: '#f5f5f5',
    borderRadius: 25,
    paddingLeft: 30,
  },
  icon: {position: 'absolute', right: 15, top: 9},
  result: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 20,
    justifyContent: 'flex-start',
  },
  q: {marginHorizontal: 5},
  img: {width: 108, height: 192},
  txtTitle: {
    color: '#2d3035',
    marginTop: 5,
    marginBottom: 10,
    width: 108,
  },
});
