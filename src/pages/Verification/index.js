import React, {Component} from 'react';
import {Text, View, Button, TouchableOpacity} from 'react-native';
import {Styles} from './style';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import LottieView from 'lottie-react-native';

export class Verification extends Component {
  constructor() {
    super();
    this.state = {time: {}, seconds: 6};
    this.timer = 0;
    this.startTimer = this.startTimer.bind(this);
    this.countDown = this.countDown.bind(this);
  }

  secondsToTime = secs => {
    let hours = Math.floor(secs / (60 * 60));

    let divisor_for_minutes = secs % (60 * 60);
    let minutes = Math.floor(divisor_for_minutes / 60);

    let divisor_for_seconds = divisor_for_minutes % 60;
    let seconds = Math.ceil(divisor_for_seconds);

    let obj = {
      h: hours,
      m: minutes,
      s: seconds,
    };
    return obj;
  };
  componentDidMount() {
    this.animation.play();
    let timeLeftVar = this.secondsToTime(this.state.seconds);
    this.setState({time: timeLeftVar});
    this.startTimer();
    setTimeout(() => {
      this.props.navigation.replace('Login');
    }, 7000);
  }

  startTimer = () => {
    if (this.timer === 0 && this.state.seconds > 0) {
      this.timer = setInterval(this.countDown, 1000);
    }
  };

  countDown = () => {
    let seconds = this.state.seconds - 1;
    this.setState({
      time: this.secondsToTime(seconds),
      seconds: seconds,
    });
    if (seconds == 0) {
      clearInterval(this.timer);
    }
  };

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  render() {
    return (
      <View style={Styles.container}>
        <Text style={Styles.success}> Registration Success </Text>
        <LottieView
          style={Styles.anim}
          ref={animation => {
            this.animation = animation;
          }}
          source={require('../../assets/lottie/e-mail.json')}
        />
        <Text style={Styles.check}>
          Please check your email for email verification
        </Text>
        <Text style={Styles.redirect}>
          Redirecting to login page in: {this.state.time.s} s
        </Text>
        <TouchableOpacity
          style={Styles.buttonBack}
          title="back to login screen"
          onPress={() => this.props.navigation.replace('Login')}>
          <Text style={Styles.backText}>Back to login screen</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Verification;
