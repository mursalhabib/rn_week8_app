import {StyleSheet} from 'react-native';
import {COLORS} from '../../constant/colors';
import {FONTS} from '../../constant/fonts';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';

export const Styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.WHITE,
    paddingHorizontal: scale(20),
  },
  anim: {width: scale(250), height: verticalScale(250)},
  success: {
    color: COLORS.BLACK,
    fontSize: moderateScale(25),
    marginBottom: verticalScale(80),
    fontFamily: FONTS.BLACK,
  },
  redirect: {
    color: COLORS.BLACK,
    marginBottom: verticalScale(7),
    fontFamily: FONTS.REGULAR,
  },
  check: {
    color: COLORS.BLACK,
    fontSize: moderateScale(17),
    marginBottom: verticalScale(180),
    fontFamily: FONTS.BLACK,
  },
  buttonBack: {
    backgroundColor: COLORS.TEAL,
    paddingHorizontal: scale(90),
    paddingVertical: verticalScale(10),
    borderRadius: 15,
  },
  backText: {
    color: COLORS.WHITE,
    fontSize: moderateScale(15),
    fontFamily: FONTS.BLACK,
  },
});
