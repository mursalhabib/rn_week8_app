import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../constant/colors';
import {FONTS} from '../../constant/fonts';

const winHeight = Dimensions.get('window').height;
export const Styles = StyleSheet.create({
  con: {
    flex: 1,
    backgroundColor: COLORS.WHITE,
    paddingHorizontal: 10,
  },
  icon: {marginLeft: 'auto'},
  profCointainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 20,
  },
  imgFlatList: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  placeholder: {
    height: 192,
    width: 108,
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: COLORS.LIGHT_DARK,
  },
  textHola: {color: COLORS.DARK, marginLeft: 10},
  txtRecommended: {
    color: COLORS.DARK,
    fontSize: 21,
    fontFamily: FONTS.BOLD,
    marginTop: 20,
    marginLeft: 6,
  },
  bookContainer: {
    flex: 1,
    marginTop: 20,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
  },
  bookImg: {
    width: 117,
    height: 153,
    marginHorizontal: 6,
    borderRadius: 10,
  },
  bookTitle: {
    color: COLORS.BLACK,
    width: 117,
    fontSize: 13,
    textAlign: 'center',
    alignSelf: 'center',
  },
  loader: {
    height: 153,
    width: 117,
    backgroundColor: COLORS.LIGHT_GRAY,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    height: 200,
    backgroundColor: COLORS.DARK,
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    paddingTop: 10,
  },
  rbsContainer: {paddingHorizontal: 20, flex: 1},
  textLogout: {color: COLORS.WHITE, fontSize: 20, marginBottom: 50},
  btnContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  cancel: {color: COLORS.LIGHT_GRAY},
  logout: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: COLORS.WHITE,
    borderRadius: 12,
    marginLeft: 20,
  },
  bar: {
    height: 5,
    width: 40,
    backgroundColor: COLORS.LIGHT_GRAY,
    alignSelf: 'center',
    borderRadius: 5,
    marginBottom: 30,
  },
});
