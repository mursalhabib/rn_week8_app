import React, {Component} from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  RefreshControl,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import {Image} from 'react-native-elements';
import {connect} from 'react-redux';
import {logoutAction} from 'action/LoginAction';
import {
  getAllBookList,
  getMoreBook,
  isCloseToBottom,
} from 'action/BookListAction';
import {bookmarkBook} from 'action/BookmarkAction';
import {truncate} from 'action/SearchAction';
import Icon from 'react-native-vector-icons/Ionicons';
import Dot from 'react-native-vector-icons/MaterialCommunityIcons';
import RBSheet from 'react-native-raw-bottom-sheet';
import {Styles} from './style';
import Loading from './Loading';
import {COLORS} from 'colors';

export class Home extends Component {
  componentDidMount() {
    this.props.getMoreBook();
    this.props.bookmarkBook();
  }

  onLogout = () => {
    this.props.logoutAction(this.props.history);
    this.props.navigation.replace('Login');
  };

  handleLoadMore = () => {
    this.props.getMoreBook(this.props.page);
  };

  recommendBooks = ({item}) => {
    return (
      <TouchableOpacity
        activeOpacity={0.6}
        onPress={() => this.props.navigation.navigate('Detail', item.id)}
        style={Styles.imgFlatList}>
        <Image
          source={{uri: item.cover_image}}
          style={Styles.placeholder}
          PlaceholderContent={<ActivityIndicator />}
        />
      </TouchableOpacity>
    );
  };
  render() {
    console.log(this.props.bookData, 'ini data buku');
    return (
      <ScrollView
        style={Styles.con}
        onScroll={({nativeEvent}) => {
          if (this.props.isCloseToBottom(nativeEvent)) {
            this.handleLoadMore();
          }
        }}
        refreshControl={
          <RefreshControl
            refreshing={this.props.load}
            onRefresh={() => this.props.getAllBookList()}
          />
        }>
        {this.props.load && this.props.page === 1 ? (
          <Loading />
        ) : (
          <View>
            <View style={Styles.profCointainer}>
              <Icon
                name="md-person-circle-outline"
                size={30}
                color={COLORS.TEAL}
              />
              <Text style={Styles.textHola}>
                Hola, {this.props.userData.data.user.name}
              </Text>
              <TouchableOpacity
                onPress={() => this.RBSheet.open()}
                style={Styles.icon}>
                <Icon name="md-log-out-outline" size={25} color={COLORS.GRAY} />
              </TouchableOpacity>
            </View>
            <View>
              <Text style={Styles.txtRecommended}>Recommended</Text>
              <FlatList
                data={this.props.bookData}
                renderItem={this.recommendBooks}
                keyExtractor={(i, k) => k.toString()}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              />
            </View>
            <View>
              <Text style={Styles.txtRecommended}>Popular Books</Text>
              <View style={Styles.bookContainer}>
                {this.props.bookData.map(item => (
                  <TouchableOpacity
                    style={{
                      height: 220,
                    }}
                    key={item.id}
                    onPress={() =>
                      this.props.navigation.navigate('Detail', item.id)
                    }>
                    <Image
                      source={{uri: item.cover_image}}
                      style={Styles.bookImg}
                      PlaceholderContent={<ActivityIndicator />}
                    />
                    <Text style={Styles.bookTitle}>
                      {this.props.truncate(item.title)}
                    </Text>
                  </TouchableOpacity>
                ))}
                {this.props.load ? (
                  <View style={Styles.loader}>
                    <Dot
                      name="dots-horizontal"
                      color={COLORS.WHITE}
                      size={50}
                    />
                  </View>
                ) : (
                  <></>
                )}
              </View>
            </View>
            <RBSheet
              ref={ref => {
                this.RBSheet = ref;
              }}
              openDuration={250}
              customStyles={{
                container: Styles.container,
              }}>
              <View style={Styles.bar} />
              <View style={Styles.rbsContainer}>
                <Text style={Styles.textLogout}>
                  Log out from your account?{' '}
                </Text>
                <View style={Styles.btnContainer}>
                  <TouchableOpacity onPress={() => this.RBSheet.close()}>
                    <Text style={Styles.cancel}>cancel</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={this.onLogout}
                    style={Styles.logout}>
                    <Text>logout</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </RBSheet>
          </View>
        )}
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  const userData = state.login.user;
  const bookData = state.allBook.books;
  const page = state.allBook.page;
  const load = state.allBook.isLoading;
  return {
    userData,
    bookData,
    load,
    page,
  };
};

const mapDispatchToProps = {
  getAllBookList,
  logoutAction,
  getMoreBook,
  truncate,
  isCloseToBottom,
  bookmarkBook,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
