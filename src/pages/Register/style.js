import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../constant/colors';
import {FONTS} from '../../constant/fonts';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';

const winHeight = Dimensions.get('window').height;
export const Styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.WHITE,
    paddingHorizontal: scale(40),
    paddingBottom: verticalScale(150),
  },
  form: {
    marginTop: winHeight * verticalScale(0.2),
  },
  textRegister: {
    color: COLORS.BLACK,
    fontSize: moderateScale(22),
    marginBottom: verticalScale(25),
    fontFamily: FONTS.BLACK,
  },
  nameInput: {
    backgroundColor: COLORS.LIGHT_DARK,
    padding: moderateScale(10),
    borderRadius: 17,
    paddingLeft: scale(30),
    color: COLORS.BLACK,
    fontSize: moderateScale(17),
    marginBottom: verticalScale(15),
  },
  emailInput: {
    backgroundColor: COLORS.LIGHT_DARK,
    padding: moderateScale(10),
    borderRadius: 17,
    paddingLeft: scale(30),
    color: COLORS.BLACK,
    fontSize: moderateScale(17),
    marginBottom: verticalScale(15),
  },
  passwordInput: {
    flex: 1,
    backgroundColor: COLORS.LIGHT_DARK,
    padding: moderateScale(10),
    borderRadius: 17,
    paddingLeft: scale(30),
    color: COLORS.BLACK,
    fontSize: moderateScale(17),
    marginBottom: verticalScale(15),
  },
  question: {
    marginTop: winHeight * verticalScale(0.25),
    flexDirection: 'row',
    justifyContent: 'center',
  },
  buttonRegisterDisabled: {
    backgroundColor: COLORS.LIGHT_TEAL,
    alignItems: 'center',
    justifyContent: 'center',
    padding: moderateScale(7),
    height: verticalScale(50),
    borderRadius: 15,
    marginTop: verticalScale(15),
  },
  buttonRegister: {
    backgroundColor: COLORS.TEAL,
    alignItems: 'center',
    justifyContent: 'center',
    padding: moderateScale(7),
    height: verticalScale(50),
    borderRadius: 15,
    marginTop: verticalScale(15),
  },
  textBtnLogin: {
    fontSize: moderateScale(17),
    color: COLORS.WHITE,
    fontFamily: FONTS.BLACK,
  },
  quest: {
    color: COLORS.BLACK,
    fontFamily: FONTS.REGULAR,
  },
  quest2: {
    color: COLORS.BLACK,
    fontFamily: FONTS.BLACK,
  },
  passwordContainer: {flexDirection: 'row'},
  btnToggle: {position: 'absolute', right: scale(20), top: verticalScale(10)},
  val: {
    position: 'absolute',
    top: winHeight * verticalScale(0.24),
    left: scale(10),
    color: COLORS.RED,
    fontSize: moderateScale(11),
  },
  val2: {
    position: 'absolute',
    top: winHeight * verticalScale(0.33),
    left: scale(10),
    color: COLORS.RED,
    fontSize: moderateScale(11),
  },
});
