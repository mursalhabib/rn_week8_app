import React, {Component} from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import {connect} from 'react-redux';
import {getDataRegister} from 'action/RegisterAction';
import {Styles} from './style';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {COLORS} from 'colors';

export class Register extends Component {
  constructor() {
    super();
    this.state = {
      showPassword: true,
    };
  }
  handleChangeName = name => {
    this.setState({name});
  };
  handleChangeEmail = email => {
    this.setState({email});
  };

  handleChangePassword = password => {
    this.setState({password});
  };

  handleSubmitItem = () => {
    const dataBody = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
    };
    this.props.getDataRegister(dataBody);
    console.log(dataBody);
  };

  validationEmail = email => {
    var regex = /^([a-z\d\.-]+)@([a-z\d\.-]+)\.([a-z]{2,8})(\.[a-z{2,8}])?$/;
    return regex.test(email);
  };
  validationPassword = password => {
    var regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/g;
    return regex.test(password);
  };

  componentDidUpdate() {
    if (this.props.user.success === true) {
      this.props.navigation.replace('Verification');
    }
  }

  togglePassword = () => {
    this.setState({showPassword: !this.state.showPassword});
  };

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={Styles.container}>
          <View style={Styles.form}>
            <Text style={Styles.textRegister}>Register</Text>

            <TextInput
              style={Styles.nameInput}
              placeholder="name"
              value={this.state.name}
              onChangeText={this.handleChangeName}
              onSubmitEditing={() => {
                this.secondTextInput.focus();
              }}
              blurOnSubmit={false}
              returnKeyType={'next'}
            />
            <TextInput
              ref={input => {
                this.secondTextInput = input;
              }}
              style={Styles.emailInput}
              placeholder="email"
              value={this.state.email}
              onChangeText={this.handleChangeEmail}
              onSubmitEditing={() => {
                this.thirdTextInput.focus();
              }}
              blurOnSubmit={false}
              returnKeyType={'next'}
            />

            {!this.state.email || this.validationEmail(this.state.email) ? (
              <></>
            ) : (
              <Text style={Styles.val}>
                email must be in correct email format
              </Text>
            )}

            <View style={Styles.passwordContainer}>
              <TextInput
                ref={input => {
                  this.thirdTextInput = input;
                }}
                style={Styles.passwordInput}
                placeholder="password"
                secureTextEntry={this.state.showPassword}
                value={this.state.password}
                onChangeText={this.handleChangePassword}
              />
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={this.togglePassword}
                style={Styles.btnToggle}>
                {this.state.showPassword === true ? (
                  <Icon name="eye-off-outline" size={25} color={COLORS.TEAL} />
                ) : (
                  <Icon name="eye-outline" size={25} color={COLORS.TEAL} />
                )}
              </TouchableOpacity>
            </View>

            {!this.state.password ||
            this.validationPassword(this.state.password) ? (
              <></>
            ) : (
              <Text style={Styles.val2}>
                password min. 8 characters with at least a capital and number
              </Text>
            )}
          </View>
          <View style={Styles.question}>
            <Text style={Styles.quest}>Already have an account? </Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.replace('Login')}>
              <Text style={Styles.quest2}>Sign in</Text>
            </TouchableOpacity>
          </View>

          {!this.validationEmail(this.state.email) ||
          !this.state.name ||
          !this.validationPassword(this.state.password) ? (
            <View>
              <TouchableOpacity
                style={Styles.buttonRegisterDisabled}
                disabled={true}>
                <Text style={Styles.textBtnLogin}>Register</Text>
              </TouchableOpacity>
            </View>
          ) : (
            <View>
              <TouchableOpacity
                onPress={this.handleSubmitItem}
                style={Styles.buttonRegister}>
                <Text style={Styles.textBtnLogin}>Register</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.login,
  };
};

const mapDispatchToProps = {
  getDataRegister,
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
