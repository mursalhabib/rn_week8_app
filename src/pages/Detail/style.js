import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../constant/colors';
import {FONTS} from '../../constant/fonts';

const winHeight = Dimensions.get('window').height;
const winWidth = Dimensions.get('window').width;
export const Styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imgCon: {
    position: 'absolute',
    width: winWidth,
    height: winHeight * 0.5,
    backgroundColor: COLORS.LIGHT_GRAY_100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  coverImg: {
    height: 242,
    width: 158,
  },
  infoContainer: {
    marginTop: winHeight * 0.46,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: COLORS.WHITE,
    paddingHorizontal: 20,
    paddingBottom: 30,
  },
  stars: {flexDirection: 'row', marginTop: 10, marginBottom: 20},
  moreCon: {
    flexDirection: 'row',
    borderRadius: 10,
    backgroundColor: COLORS.LIGHT_GRAY_100,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  ratingCon: {
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  txtRating: {textAlign: 'center', fontFamily: FONTS.REGULAR},
  ratingNum: {
    fontSize: 17,
    color: COLORS.BLACK,
    marginRight: 10,
    textAlign: 'center',
    fontFamily: FONTS.BOLD,
  },
  pageCon: {
    padding: 15,
    paddingHorizontal: 30,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderColor: COLORS.LIGHT_DARK,
  },
  txtPage: {textAlign: 'center', fontFamily: FONTS.REGULAR},
  pageNum: {
    textAlign: 'center',
    fontFamily: FONTS.BOLD,
    fontSize: 17,
  },
  priceCon: {
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  priceBtn: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.TEAL,
    padding: 10,
    borderRadius: 10,
  },
  txtPrice: {
    fontFamily: FONTS.BOLD,
    color: '#fff',
    fontSize: 15,
  },
  titleSynopsis: {
    color: COLORS.BLACK,
    marginTop: 30,
    fontFamily: FONTS.BOLD,
    fontSize: 18,
  },
  txtSynopsis: {
    color: COLORS.BLACK,
    marginTop: 10,
    fontFamily: FONTS.REGULAR,
    textAlign: 'justify',
  },
  title: {
    color: COLORS.BLACK,
    marginTop: 30,
    fontSize: 20,
    fontFamily: FONTS.BOLD,
  },
  author: {
    color: COLORS.BLACK,
    marginTop: 10,
    fontFamily: FONTS.REGULAR,
    fontSize: 16,
  },
});
