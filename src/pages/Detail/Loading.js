import React, {Component} from 'react';
import {Text, View, Dimensions} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

export class Loading extends Component {
  render() {
    const winHeight = Dimensions.get('window').height;
    const winWidth = Dimensions.get('window').width;
    return (
      <View>
        <SkeletonPlaceholder>
          <View
            style={{
              marginTop: 20,
            }}>
            <View
              style={{
                width: 158,
                height: 242,
                borderRadius: 4,
                marginTop: winHeight * 0.04,
                marginLeft: winWidth * 0.3,
              }}
            />
          </View>
          <View style={{marginTop: 60, marginLeft: winWidth * 0.05}}>
            <View
              style={{
                width: winWidth * 0.9,
                height: 20,
              }}
            />
            <View
              style={{
                width: winWidth * 0.35,
                height: 15,
                marginTop: 7,
              }}
            />
            <View
              style={{
                width: winWidth * 0.25,
                height: 25,
                marginTop: 7,
              }}
            />
            <View
              style={{
                width: winWidth * 0.85,
                height: 70,
                marginTop: 20,
                marginLeft: 10,
              }}
            />
            <View
              style={{
                width: winWidth * 0.35,
                height: 25,
                marginTop: 20,
              }}
            />
            <View
              style={{
                width: winWidth * 0.9,
                height: 100,
                marginTop: 10,
              }}
            />
          </View>
        </SkeletonPlaceholder>
      </View>
    );
  }
}

export default Loading;
