import React, {Component} from 'react';
import {
  RefreshControl,
  Text,
  View,
  ActivityIndicator,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {getDataLogin} from 'action/LoginAction';
import {connect} from 'react-redux';
import {detailBook} from 'action/DetailBookAction';
import {Styles} from './style';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Image} from 'react-native-elements';
import Stars from 'react-native-stars';
import NumberFormat from 'react-number-format';
import Loading from './Loading';
import {COLORS} from 'colors';

export class Detail extends Component {
  componentDidMount() {
    this.props.detailBook(this.props.route.params);
  }

  render() {
    const star = parseInt(this.props.bookDetail.average_rating) / 2;
    const price = parseInt(this.props.bookDetail.price);
    return (
      <ScrollView
        style={Styles.container}
        refreshControl={
          <RefreshControl
            refreshing={this.props.load}
            onRefresh={() => this.props.detailBook(this.props.route.params)}
          />
        }>
        {this.props.load ? (
          <Loading />
        ) : (
          <>
            <View style={Styles.imgCon}>
              <Image
                source={{uri: this.props.bookDetail.cover_image}}
                style={Styles.coverImg}
                PlaceholderContent={<ActivityIndicator />}
              />
            </View>
            <View style={Styles.infoContainer}>
              <Text style={Styles.title}>{this.props.bookDetail.title}</Text>
              <Text style={Styles.author}>{this.props.bookDetail.author}</Text>
              <View style={Styles.stars}>
                <Stars
                  disabled={true}
                  rating={star}
                  count={5}
                  half={true}
                  fullStar={
                    <Icon name="star" size={20} color={COLORS.ORANGE} />
                  }
                  emptyStar={
                    <Icon name="star-o" size={20} color={COLORS.ORANGE} />
                  }
                  halfStar={
                    <Icon
                      name="star-half-full"
                      size={20}
                      color={COLORS.ORANGE}
                    />
                  }
                />
              </View>
              <View style={Styles.moreCon}>
                <View style={Styles.ratingCon}>
                  <Text style={Styles.txtRating}>Rating:</Text>
                  <Text style={Styles.ratingNum}>
                    {this.props.bookDetail.average_rating}
                  </Text>
                </View>

                <View style={Styles.pageCon}>
                  <Text style={Styles.txtPage}>Page:</Text>
                  <Text style={Styles.pageNum}>
                    {this.props.bookDetail.page_count}
                  </Text>
                </View>
                <View style={Styles.priceCon}>
                  <TouchableOpacity activeOpacity={0.7} style={Styles.priceBtn}>
                    <NumberFormat
                      value={price}
                      displayType={'text'}
                      thousandSeparator={true}
                      prefix={'Rp. '}
                      renderText={(value, props) => (
                        <Text {...props} style={Styles.txtPrice}>
                          {value}
                        </Text>
                      )}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <Text style={Styles.titleSynopsis}>Synopsis</Text>
              <Text style={Styles.txtSynopsis}>
                {this.props.bookDetail.synopsis}
              </Text>
            </View>
          </>
        )}
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  const userData = state.login.user;
  const bookDetail = state.allBook.detailBook;
  const load = state.allBook.isLoading;
  return {
    userData,
    bookDetail,
    load,
  };
};

const mapDispatchToProps = {
  getDataLogin,
  detailBook,
};

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
